from werkzeug.exceptions import NotFound

from models.database import db
from sqlalchemy import Column, Integer, String, Text, \
    DateTime, ForeignKey, Boolean
from sqlalchemy.orm import relationship
from sqlalchemy.sql import func


class User(db.Model):
    """
    Таблица "Пользователи"
    """
    __tablename__ = "users"

    id = Column(Integer, primary_key=True)
    username = Column(String(50), unique=True, nullable=False)
    firstname = Column(String(50), nullable=False)
    lastname = Column(String(50), nullable=False)

    user_posts = relationship("Post",
                              back_populates="user",
                              cascade="all, delete",
                              passive_deletes=True)

    def __str__(self):
        return f"{self.firstname} {self.lastname}"


class Post(db.Model):
    """
    Таблица "Посты"
    """
    __tablename__ = "posts"

    id = Column(Integer, primary_key=True)
    title = Column(String(250), nullable=False)
    description = Column(Text, nullable=False)
    in_archive = Column(Boolean, server_default="0")
    created_at = Column(DateTime, server_default=func.now())
    updated_at = Column(DateTime, server_default=func.now())
    user_id = Column(Integer, ForeignKey("users.id", ondelete='CASCADE'), nullable=False)
    user = relationship("User", back_populates="user_posts")

    post_links = relationship("PostTagLink",
                              back_populates="post_link",
                              cascade="all, delete",
                              passive_deletes=True)

    @classmethod
    def get_post_by_id(cls, post_id: int, exclude_archive: bool = True):
        """
        Поиск поста по его ID (post_id)

        :param post_id: ID записи
        :param exclude_archive:
            признак включать или не включать в поиск архивные записи
        """
        if exclude_archive:
            post = cls.query.filter_by(in_archive=False, id=post_id).one_or_none()
        else:
            post = cls.query.filter_by(id=post_id).one_or_none()

        if post is None:
            raise NotFound(f"Пост не найден (ID: \"{post_id}\")")
        return post

    def __str__(self):
        return f"{self.title}"

    def __repr__(self):
        return self.__str__()


class Tag(db.Model):
    """
    Таблица "Тэги"
    """
    __tablename__ = "tags"

    id = Column(Integer, primary_key=True)
    name = Column(String(40), unique=True, nullable=False)
    created_at = Column(DateTime, server_default=func.now())

    tag_links = relationship("PostTagLink",
                             back_populates="tag_link",
                             cascade="all, delete",
                             passive_deletes=True)

    @classmethod
    def get_tag_by_id(cls, tag_id: int):
        """
        Поиск тега по его ID (tag_id)

        :param tag_id: ID записи
        """
        tag = cls.query.filter_by(id=tag_id).one_or_none()

        if tag is None:
            raise NotFound(f"Пост не найден (ID: \"{tag_id}\")")
        return tag

    def __str__(self):
        return f"{self.name}"


class PostTagLink(db.Model):
    """
    Таблица связей "Пост-Тэг"
    Для реализации связи многие-ко-многим
    """
    __tablename__ = "post_tag_links"

    post_id = Column(Integer, ForeignKey("posts.id", ondelete='CASCADE'), primary_key=True)
    tag_id = Column(Integer, ForeignKey("tags.id", ondelete='CASCADE'), primary_key=True)
    created_at = Column(DateTime, server_default=func.now())

    post_link = relationship("Post", back_populates="post_links")
    tag_link = relationship("Tag", back_populates="tag_links")
