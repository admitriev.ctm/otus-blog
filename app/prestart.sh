#!/usr/bin/env sh

echo "Waiting for DB-Service"
sleep 5;

echo "Start migration"
flask db upgrade
echo "Migration complete"
