import config
from models.database import db
from flask import Flask, render_template
from flask_migrate import Migrate
from views.posts import post_app
from views.tags import tag_app
from views.users import user_app

# создание приложения и подключение дополнений (дополнительных приложений)
app = Flask(__name__)
app.register_blueprint(user_app, url_prefix="/users")
app.register_blueprint(post_app, url_prefix="/posts")
app.register_blueprint(tag_app, url_prefix="/tags")

# подключение к БД
app.config.update(
    SQLALCHEMY_DATABASE_URI=config.SQLALCHEMY_DATABASE_URI,
    SQLALCHEMY_TRACK_MODIFICATIONS=False
)
db.init_app(app)
Migrate(app, db, 'models/migrations')


@app.route("/", endpoint="index")
def index():
    return render_template("index.html")


# Команда не актуальная после подключения Flask-Migrate
# @app.cli.command("init-db", with_appcontext=True)
# def initialize_db():
#     """
#     Falsk-команда для инициализации базы данных
#     Создаёт базу данных и структуру таблиц
#     """
#     print('Start DB-init process')
#     db.create_all()
#     print('DB-init process finished')


if __name__ == "__main__":
    app.run(host='localhost', debug=True, port=5000)
