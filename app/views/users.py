from flask import Blueprint, render_template, request, redirect, url_for
from models import User
from models.database import db

user_app = Blueprint("user_app", __name__)


@user_app.route("users/", endpoint="users")
def users_list():
    users = User.query.all()
    return render_template("users/users_list.html", data=users)


@user_app.route("add/", methods=['POST', 'GET'], endpoint="add_user")
def add_user():
    if request.method == "GET":
        return render_template("users/add_user.html")

    form = request.form
    username = form.get("username")
    firstname = form.get("firstname")
    lastname = form.get("lastname")

    user = User(
        username=username,
        firstname=firstname,
        lastname=lastname
    )
    db.session.add(user)
    db.session.commit()

    return redirect(url_for("index"))
