from flask import Blueprint, render_template, request, redirect, url_for
from models import Post, User
from models.database import db
from werkzeug.exceptions import Unauthorized

post_app = Blueprint("post_app", __name__)


@post_app.route("/", endpoint="posts")
def posts_list():
    posts = Post.query.filter_by(in_archive=False)
    return render_template("blog/posts_list.html", data=posts, data_length=posts.count())


@post_app.route("/<int:post_id>/", endpoint="post_detail")
def post_detail(post_id):
    post = Post.get_post_by_id(post_id)
    return render_template("blog/post.html", post=post)


@post_app.route("/<int:post_id>/archive", methods=["POST"], endpoint="archive_post")
def archive_post(post_id):
    post = Post.get_post_by_id(post_id)
    post.in_archive = True
    db.session.commit()

    return redirect(url_for("post_app.posts"))


@post_app.route("add/", methods=['POST', 'GET'], endpoint="add_post")
def add_post():
    if request.method == "GET":
        users = User.query.all()
        return render_template("blog/add_post.html", users=users)

    form = request.form
    title = form.get("post_title")
    description = form.get("post_description")
    user_id = form.get("post_author")

    if user_id is None:
        raise Unauthorized(
            "Ошибка создания поста! Авторизуйтесь и попробуйте снова.")

    # создаём пост и сохраняем его в базу данных
    post = Post(
        title=title,
        description=description,
        user_id=user_id)
    db.session.add(post)
    db.session.commit()

    return redirect(url_for("post_app.posts"))
