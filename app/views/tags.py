from flask import Blueprint, render_template, request, redirect, url_for
from models import Tag
from models.database import db

tag_app = Blueprint("tag_app", __name__)


@tag_app.route("/", endpoint="tags")
def tags_list():
    tags = Tag.query.all()
    return render_template("tags/tags_list.html", data=tags)


@tag_app.route("/<int:tag_id>/", endpoint="tag_detail")
def tag_details(tag_id):
    tag = Tag.get_tag_by_id(tag_id)
    return render_template("tags/tag.html", tag=tag)


@tag_app.route("/<int:tag_id>/delete", methods=["POST"], endpoint="delete_tag")
def archive_post(tag_id):
    tag = Tag.get_tag_by_id(tag_id)
    db.session.delete(tag)
    db.session.commit()

    return redirect(url_for("tag_app.tags"))


@tag_app.route("add/", methods=['POST', 'GET'], endpoint="add_tag")
def add_tag():
    if request.method == "GET":
        return render_template("tags/add_tag.html")

    form = request.form
    name = form.get("tag_name")

    tag = Tag(name=name)
    db.session.add(tag)
    db.session.commit()

    return redirect(url_for("tag_app.tags"))
